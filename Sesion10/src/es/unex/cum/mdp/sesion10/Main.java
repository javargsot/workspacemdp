package es.unex.cum.mdp.sesion10;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frmTitulo;
	private JTextField tFValor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmTitulo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTitulo = new JFrame();
		frmTitulo.setTitle("titulo");
		frmTitulo.setBounds(100, 100, 528, 358);
		frmTitulo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel lblParidad = new JLabel("Paridad");
		tFValor = new JTextField();
		
		JPanel panel = new JPanel();
		frmTitulo.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton botonTitulo = new JButton("Cambiar Titulo");
		botonTitulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmTitulo.setTitle("Paridad");
			}
		});
		panel.add(botonTitulo);
		
		JButton botonValor = new JButton("Cambiar valor");
		panel.add(botonValor);
		
		JButton botonLabel = new JButton("Cambiar Etiqueta");
		botonLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Integer valor = Integer.parseInt(tFValor.getText());
					if(valor%2 == 0) {
						lblParidad.setText("Par");
					}else {
						lblParidad.setText("Impar");
					}
				
				}catch(NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		});
		panel.add(botonLabel);
		
		JPanel panel_1 = new JPanel();
		frmTitulo.getContentPane().add(panel_1, BorderLayout.NORTH);
		
		panel_1.add(lblParidad);
		
		
		tFValor.setHorizontalAlignment(SwingConstants.CENTER);
		tFValor.setToolTipText("valor");
		tFValor.setText("0");
		panel_1.add(tFValor);
		tFValor.setColumns(10);
	}

}
