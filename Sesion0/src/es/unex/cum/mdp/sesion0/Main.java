package es.unex.cum.mdp.sesion0;

import java.io.IOException;

public class Main {
	
	private Teclado t = new Teclado();
	private Parking[] v;

	public static void main(String[] args) throws IOException {

		Main m = new Main();
		m.run();
		
	}
	
	public void run() throws IOException{
		
		inicializar();
		rellenar();
		mostrar();
		media();
		busca();
		mayor();
		
	}
	
	public void inicializar() throws IOException{
		
		v = new Parking[t.leerEntero()];
		
	}
	
	public void rellenar() throws IOException{
		
		for(int i = 0 ; i<v.length ; i++){
			
			String n = t.leerLinea();
			int pl = t.leerEntero();
			double pz = t.leerDouble();
			
			v[i] = new Parking(n,pl,pz);
			
		}
		
	}
	
	public void mostrar(){
		
		for(int i = 0 ; i < v.length ; i++){
			System.out.println(v[i].toString());
		}
		
	}
	
	public void media(){
		
		double suma = 0;
		double media = 0.0;
		
		for(int i = 0 ; i < v.length ; i++){
			suma = suma + v[i].getPlazas();
		}
		
		media = suma/v.length;
		
		System.out.println("Suma: " + (int)suma + " Media: " + media);
		
	}
	
	public void busca() throws IOException{
		
		double b = t.leerDouble();
		boolean en = false;
		int i = 0;
		
		while(i < v.length && en == false){
			if(b==v[i].getPrecio()){
				en = true;
			i++;
			}
			
			if(en == true){
				System.out.println("El valor " + b + " se encuentra en el array");
			}else{
				System.out.println("El valor " + b + " no se encuentra en el array");
			}		
		
		}
		
	}
	
	public void mayor(){
		
		Parking mayor = new Parking();
		int pos = 0;
		
		for(int i = 0 ; i < v.length ; i++){
			if(mayor.getPlazas()<=v[i].getPlazas()){
				mayor = v[i];
				pos = i;
			}
		}
		
		System.out.println("El valor mas alto es "+ mayor.getPlazas() +" y esta en la posicion " + pos);
		
	}

}
