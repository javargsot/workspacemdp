package es.unex.cum.mdp.sesion0;

public class Parking {
	
	private String nombre;
	private int plazas;
	private double precio;
	
	public Parking(){
		
		this.nombre = "Aparcamiento";
		this.plazas = 0;
		this.precio = 0.0;
		
	}
	
	public Parking(String n, int pl, double pr){
		
		nombre = n;
		plazas = pl;
		precio = pr;
		
	}
	
	public boolean equals(Object o){
		
		Parking p = (Parking) o;
				
		return nombre.equals(p.getNombre())&&plazas==p.getPlazas()&&precio==p.getPrecio();
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(Integer plazas) {
		this.plazas = plazas;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	public String toString(){
		return"[" + nombre + ", " + plazas + ", " + precio + "]";
	}

}
