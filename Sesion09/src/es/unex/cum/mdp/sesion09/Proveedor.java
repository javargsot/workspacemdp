package es.unex.cum.mdp.sesion09;

import java.io.Serializable;

public class Proveedor extends Persona implements Serializable {

	private Integer telf;
	
	public Proveedor() {
		super();
		telf = 0;
		
	}
	
	public Proveedor(String nombre, String dni, Integer edad, Integer telf) {
		super(nombre, dni, edad);
		
		this.telf = telf;
	}
	
	public Proveedor(Proveedor p) {
		super(p);
		
		telf = p.getTelf();
	}

	public Integer getTelf() {
		return telf;
	}

	public void setTelf(Integer telf) {
		this.telf = telf;
	}
	
	
	
}
