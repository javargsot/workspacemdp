package es.unex.cum.mdp.sesion09;

public class ExceptionBastidorNoEncontrado extends Exception {
	private static final long serialVersionUID = 2L;

	public ExceptionBastidorNoEncontrado() {
		super();
	}

	public ExceptionBastidorNoEncontrado(String msg) {
		super(msg);
	}

	public String getMessage() {
		return "Error debido a que el vector esta Vacio: " + super.getMessage();
	}
}
