package es.unex.cum.mdp.sesion09;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.TreeSet;

public class Main {
	private Desguace d;

	public static void main(String[] args) {
		Main m = new Main();
		m.ejecutar();
	}

	public void ejecutar() {

		try {
			cargarSerializable();
		} catch (Exception e2) {
			System.out.println("Error");
			d = new Desguace();
		}
		
		try {
			cargarPiezas();
		} catch (NumberFormatException | IOException e1) {
			System.out.println();
		}
		Teclado t = new Teclado();
		int opcion = 0;
		Vehiculo v = null;
		do {
			try {
				opcion = t.Menu(new String[] { "1. Añadir Pieza", "2. Añadir Vehiculo", "3. Añadir Pieza Vehiculo",
						"4. Mostrar Piezas/Vehiculo", "5. Salir" }, 1, 5);
				switch (opcion) {
				case 1:
					String id = t.literalConString("Dame el id");
					try {
						Pieza p2 = d.getPiezaDesguace(id);
						System.out.println("que ya esta");
					} catch (ExceptionPiezaNoEncontrada e) {
						String nombre = t.literalConString("Dame elnombre");
						int stock = t.literalConEntero("Dame el número");
						Pieza paux = new Pieza(id, nombre, stock);
						d.addPiezaDesguace(paux);
					}
					break;
				case 2:
					Integer bastidor = t.literalConEntero("Dame el bástidor");
					String marca = t.literalConString("Dame el marca");
					String modelo = t.literalConString("Dame el modelo");
					int tam = t.literalConEntero("Dame el tamanio de piezas de vehiculo");
					int tipo = t.literalConEntero("1. Coche 2. Moto 3.Camion");
					while (tipo < 1 || tipo > 3) {
						tipo = t.literalConEntero("1. Coche 2. Moto 3.Camion");
					}
					switch (tipo) {
					case 1:
						String c = t.literalConString("Dame el color");
						v = new Coche(marca, modelo, new Persona(), bastidor, c);
						break;
					case 2:
						Integer i = (Integer) t.literalConEntero("Dame la potencia");
						v = new Moto(marca, modelo, new Persona(), bastidor, i);
						break;
					case 3:
						Integer a = (Integer) t.literalConEntero("Dame el tonelaje");
						v = new Camion(marca, modelo, new Persona(), bastidor, a);
					}
					d.addVehiculo(v);
					break;
				case 3:
					String id2 = t.literalConString("Dame el id");
					Integer b2 = t.literalConEntero("Dame el bastidor");
					// d.addPiezaVehiculo(id2, b2);
					break;
				case 4:
					System.out.println(d.getVehiculos().toString());
					break;

				case 5:
					salvarPiezas();
					salvarSerializable();
					break;
				}
			} catch (IOException e) {
				opcion = 0;
				System.out.println("Excepción");
			}
		} while (opcion != 5);
	}

	public void cargarPiezas() throws IOException, NumberFormatException {
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(new FileInputStream("Persona.txt")));
		String line = null;
		int cont = 0;
		TreeSet<Pieza> aux = new TreeSet<Pieza>();
		d.setPiezas(aux);

		while ((line = br.readLine()) != null) { // Leo línea a línea
			// divido la línea por cada : que presenta un nombre:dni:edad
			String[] split = line.split(",");
			Pieza p = new Pieza(split[0], split[1], Integer.parseInt(split[2]));
			d.addPiezaDesguace(p);
		}
		br.close();
	}

	public void salvarPiezas() {
		BufferedWriter br = null;
		try {
			br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Persona.txt")));
			// Recorrer el array de Empleados
			Iterator it = d.getPiezas().iterator();
			while (it.hasNext()) {
				Pieza e = (Pieza) it.next();
				br.write(e.getId() + "," + e.getNombre() + "," + e.getStock() + "\n");
			}
			br.close();
		} catch (Exception ex) {
		}
	}

	public void salvarSerializable() throws FileNotFoundException, IOException {
		ObjectOutputStream archivoObjetosSal = new ObjectOutputStream(new FileOutputStream("datos"));
		archivoObjetosSal.writeObject(d); // donde XXXX es la inf. a salvar
		archivoObjetosSal.close();
	}

	public void cargarSerializable() throws Exception {
		ObjectInputStream archivoObjetosEnt = new ObjectInputStream(new FileInputStream("datos"));
		d = (Desguace) archivoObjetosEnt.readObject();
		archivoObjetosEnt.close();
	}
}