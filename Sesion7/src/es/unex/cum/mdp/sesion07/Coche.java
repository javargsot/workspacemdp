package es.unex.cum.mdp.sesion07;

public class Coche extends Vehiculo{
	
	private String color;
	
	public Coche() {
		super();
		color = "";
	}
	
	public Coche(String marca, String modelo, Persona propietario, Integer bastidor, String color) {
		super(marca, modelo, propietario, bastidor);
		this.color = color;
	}
	
	public Coche(Coche c) {
		super(c);
		color = c.getColor();
	}
	
	public boolean equals(Object o) {
		Coche c = (Coche) o;
		return super.equals(c)&&color.equals(c.getColor());
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public String toString() {
		return "Coche[" + super.toString() + ", Color: " + color + "]";
	} 

}
