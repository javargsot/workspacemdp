package es.unex.cum.mdp.sesion07;

public class ExceptionVectorLleno extends Exception {
	private static final long serialVersionUID = 2L;

	public ExceptionVectorLleno() {
		super();
	}

	public ExceptionVectorLleno(String msg) {
		super(msg);
	}

	public String getMessage() {
		return "Error debido a que el vector esta Vacio: " + super.getMessage();
	}
}
