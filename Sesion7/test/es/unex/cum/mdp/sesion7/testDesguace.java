package es.unex.cum.mdp.sesion7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.unex.cum.mdp.sesion07.Camion;
import es.unex.cum.mdp.sesion07.Coche;
import es.unex.cum.mdp.sesion07.Desguace;
import es.unex.cum.mdp.sesion07.Empleado;
import es.unex.cum.mdp.sesion07.ExceptionBastidorNoEncontrado;
import es.unex.cum.mdp.sesion07.ExceptionPiezaNoEncontrada;
import es.unex.cum.mdp.sesion07.ExceptionVectorLleno;
import es.unex.cum.mdp.sesion07.Moto;
import es.unex.cum.mdp.sesion07.Persona;
import es.unex.cum.mdp.sesion07.Pieza;
import es.unex.cum.mdp.sesion07.Proveedor;

public class testDesguace {

	private Desguace d1, d2, d3;
	private Moto v1;
	private Coche v2;
	private Camion v3;
	private Proveedor pr1;
	private Pieza pz1, pz2, pz3;
	private Empleado e1;
	private Persona p;

	@Before
	public void setUp() throws Exception {

		d1 = new Desguace();
		d2 = new Desguace("Desguaces Manola", 5);
		d3 = new Desguace("Desguaces La Piedra", 3);

		p = new Persona("Luis", "4", 4);

		v1 = new Moto("Kawasaki", "Ninja", p, 1, 250);
		v2 = new Coche("Mazda", "RX-8", p, 2, "Negro");
		v3 = new Camion("Iveco", "Daily", p, 3, 5);

		pr1 = new Proveedor();
		
		pz1 = new Pieza();
		pz2 = new Pieza("2", "amortiguador", 4);
		pz3 = new Pieza("4","viela",1);
		
		e1 = new Empleado();
	}

	@After
	public void tearDown() throws Exception {

		d1 = null;
		d2 = null;
		d3 = null;

		p = null;

		v1 = null;
		v2 = null;
		v3 = null;

		pr1 = null;
		
		pz1 = null;
		pz2 = null;
		pz3 = null;
		
		e1 = null;

	}

	@Test
	public void testDesguace() {
		assertNotNull(d1);
		assertNotNull(d2);
		System.out.println("testDesguace: OK");
	}

	@Test
	public void testEqualsObject() {
		d3.setNombre("Desguaces Manola");
		assertEquals(d3, d2);
		assertNotEquals(d1, d2);
		System.out.println("testEquals: OK");
	}

	@Test
	public void testGetNombre() {
		assertNotEquals("Desguaces Manola", d1.getNombre());
		assertEquals("Desguaces Manola", d2.getNombre());
		System.out.println("testGetNombre: OK");
	}

	@Test
	public void testGetTam() {
		assertNotEquals(5, d1.getTam());
		assertEquals(5, d2.getTam());
		System.out.println("testGetTam: OK");
	}

	@Test
	public void testSetNombre() {
		d3.setNombre("Desguaces El Estrellado");
		assertNotEquals(d3.getNombre(), "Desguaces Manola");
		assertEquals(d3.getNombre(), "Desguaces El Estrellado");
		System.out.println("testSetNombre: OK");
	}

	@Test
	public void testSetTam() {
		d3.setTam(10);
		assertNotEquals(d3.getTam(), 5);
		assertEquals(d3.getTam(), 10);
		System.out.println("testSetTam: OK");
	}

	@Test
	public void testAddVehiculo() {
		assertTrue(d2.addVehiculo(v1));
		assertTrue(d2.addVehiculo(v2));
		assertFalse(d2.addVehiculo(v2));

		assertTrue(d3.addVehiculo(v1));
		assertTrue(d3.addVehiculo(v2));
		assertFalse(d3.addVehiculo(v3));

		System.out.println("testAddVehiculo: OK");
	}

	@Test
	public void testGetVehiculoBastidor() {

		d2.addVehiculo(v2);
		
		try {
			
			assertEquals(d2.getVehiculoBastidor(2), v2);
			System.out.println("testGetVehiculoBastidor: OK");
			
		} catch (ExceptionBastidorNoEncontrado e) {
			
			System.out.println(e.getMessage());
		
		}

	}

	@Test
	public void testAddPiezaVehiculo() {
		assertTrue(d2.addPiezaVehiculo(pz1, 2));
		assertFalse(d2.addPiezaVehiculo(pz1, 2));
		
		try {
			
			assertTrue(d3.addPiezaVehiculo(pz1.getId(), 3));
			System.out.println("testAddPiezaVehiculo: OK");
			
		}catch(ExceptionBastidorNoEncontrado | ExceptionPiezaNoEncontrada | ExceptionVectorLleno e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}

	@Test
	public void testCuantasPiezasPar() {
		d2.addPiezaVehiculo(pz1, 2);
		assertEquals(d2.cuantasPiezasPar(), 0);
		d2.addPiezaVehiculo(pz2, 2);
		assertEquals(d2.cuantasPiezasPar(), 1);
		d2.addPiezaVehiculo(pz3, 2);
		assertEquals(d2.cuantasPiezasPar(), 1);
		System.out.println("testCuantasPiezasPar: OK");
	}

	@Test
	public void testGetInfoDerivada() {
		d3.addVehiculo(v1);
		d3.addVehiculo(v2);
		d3.addVehiculo(v3);
		assertEquals(d3.getInfoDerivada(0), v1.getPotencia());
		assertEquals(d3.getInfoDerivada(1), v2.getColor());
		assertEquals(d3.getInfoDerivada(2), v3.getTonelaje());
		System.out.println("testGetInfoDerivada: OK");
	}

	@Test
	public void testAddPiezaDesguace() {
		assertTrue(d2.addPiezaDesguace(pz1));
		System.out.println("testAddPiezaDesguace: OK");
	}

	@Test
	public void testGetPiezaDesguace() {
		d1.addPiezaDesguace(pz1);
		try {
		assertEquals(pz1, d1.getPiezaDesguace(pz1.getId()));
		System.out.println("testGetPiezaDesguace: OK");
		}catch(ExceptionPiezaNoEncontrada e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testAddProveedor() {
		assertTrue(d2.addProveedor(pr1));
		System.out.println("testAddProveedor: OK");
	}

	@Test
	public void testGetProveedor() {
		d2.addProveedor(pr1);
		assertEquals(d2.getProveedor(pr1.getDni()), pr1);
		assertNotNull(d2.getProveedor(pr1.getDni()));
		System.out.println("testGetProveedor: OK");
	}

	@Test
	public void testGetPiezaVehiculo() {
		try {
		d3.addPiezaVehiculo("2", 2);
		assertEquals("2", d3.getPiezaVehiculo("2", 2));
		System.out.println("testGetPiezaVehiculo: OK");
		}catch( ExceptionBastidorNoEncontrado | ExceptionPiezaNoEncontrada | ExceptionVectorLleno e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testMayorStock() {
		
		d2.addVehiculo(v1);
		d2.addVehiculo(v2);
		d2.addVehiculo(v3);
		
		d2.addPiezaVehiculo(pz1, 2);
		
		assertEquals(v2, d2.mayorStock());

		System.out.println("testMayorStock: OK");
		
	}

	@Test
	public void testAddEmpleado() {
		assertTrue(d3.addEmpleado(e1));
		System.out.println("testAddEmpleado: OK");
	}

	@Test
	public void testGetEmpleado() {
		d3.addEmpleado(e1);
		assertEquals(d3.getEmpleado(e1.getDni()), e1);
		System.out.println("testGetEmpleado: OK");
	}

	@Test
	public void testGetMediaSueldo() {
		d2.addEmpleado(e1);
		assertEquals(e1.getSueldo(), d2.getMediaSueldo());
		System.out.println("testGetMediaSueldo: OK");
	}

	@Test
	public void testToString() {
		assertEquals(d2.toString(), "Desguace[Nombre: Desguaces Manola]");
		System.out.println("testToString: OK");
	}

}
