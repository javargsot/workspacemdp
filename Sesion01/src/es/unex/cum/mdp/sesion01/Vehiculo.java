package es.unex.cum.mdp.sesion01;

import es.unex.cum.mdp.sesion0.Persona;

public class Vehiculo {

	private String marca;
	private String modelo;
	private Persona propietario;
	private Pieza[] piezas;
	private int cont;

	public Vehiculo() {

		marca = "";
		modelo = "";
		propietario = new Persona();
		piezas = new Pieza[5];
		cont = 0;

	}

	public Vehiculo(String marca, String modelo, Persona propietario) {

		this.marca = marca;
		this.modelo = modelo;
		this.propietario = propietario;

		piezas = new Pieza[5];
		cont = 0;

	}

	public Vehiculo(Vehiculo v) {

		marca = v.getMarca();
		modelo = v.getModelo();
		propietario = v.getPropietario();

		piezas = new Pieza[5];
		cont = 0;

	}
	
	public boolean equals(Object o) {
		
		boolean res = false;
		Vehiculo v = (Vehiculo) o;
		
		if(marca.equals(v.getMarca())) {
			if(modelo.equals(v.getModelo())) {
				if(propietario.equals(v.getPropietario())) {
					res = true;
				}
			}
		}
		
		return res;
		
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Persona getPropietario() {
		return propietario;
	}

	public void setPropietario(Persona propietario) {
		this.propietario = propietario;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public Pieza getPiezaV(int pos) {

		if (pos < 0 || pos >= piezas.length) {

			return null;

		}

		return piezas[pos];

	}

	public boolean addPiezaV(Pieza p) {

		if (cont == 0) {
			piezas[cont] = p;
			cont++;
			return true;
		} else {

//			for (int i = 0;  i < piezas.length; i++) {
//				if (piezas[i]==null) break;
//				if (p.getId().equals(piezas[i].getId())) {
//					return false;
//				}
//			}
			
			for (int i = 0; i < cont; i++) {
				
				if (p.getId().equals(piezas[i].getId())) {
					return false;
				}
			}

//			if (existeValor(p)) {
//
//				return false;
//
//			} else {
//
//				piezas[cont] = p;
//				cont++;
//
		}
		
		if(cont == piezas.length) {
			return false;
		}

		piezas[cont] = p;
		cont++;

		return true;

	}

	/*
	 * private boolean existeValor(Pieza p) {
	 * 
	 * boolean res = false;
	 * 
	 * for(Pieza p1: piezas) { if(p1.getId().equals(p.getId())) { res = true; }
	 * 
	 * }
	 * 
	 * return res;
	 * 
	 * }
	 */
	
	public String toString() {
		return "[Marca: " + marca + ", Modelo: " + modelo + ", Propietario: " + propietario + "]";
	}
	
}
