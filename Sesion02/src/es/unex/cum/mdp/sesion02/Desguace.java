package es.unex.cum.mdp.sesion02;

import es.unex.cum.mdp.sesion01.Pieza;
import es.unex.cum.mdp.sesion01.Vehiculo;

public class Desguace {

	private String nombre;
	private int tam;
	private Vehiculo[] vehiculos;
	private int cont;

	public Desguace() {

		nombre = "";
		tam = 0;

		vehiculos = new Vehiculo[0];
		cont = 0;

	}

	public Desguace(String nombre, int tam) {

		this.nombre = nombre;
		this.tam = tam;

		vehiculos = new Vehiculo[tam];
		cont = 0;

	}

	public boolean equals(Object o) {
		Desguace d = (Desguace) o;
		return nombre.equals(d.getNombre());
	}

	public String getNombre() {
		return nombre;
	}

	public int getTam() {
		return tam;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTam(int tam) {
		this.tam = tam;
	}

	public boolean addVehiculo(Vehiculo v) {

		if (cont == 0) {
			vehiculos[cont] = v;
			cont++;
			return true;
		} else {
			for (int i = 0; i < cont; i++) {
				if (v.getBastidor().equals(vehiculos[i].getBastidor())) {
					return false;
				}
			}
		}

		if (cont == tam) {
			return false;
		}

		vehiculos[cont] = v;
		cont++;

		return true;

	}

	public Vehiculo getVehiculoBastidor(Integer bastidor) {

		if (cont == 0 || cont >= tam) {
			return null;
		}

		for (int i = 0; i < cont; i++) {
			if (vehiculos[i].getBastidor().equals(bastidor)) {
				return vehiculos[i];
			}
		}

		return null;

//		if(bastidor < 0 || bastidor >= vehiculos.length) {
//			return null;
//		}

//		return vehiculos[bastidor];

	}

	// paso busco vehiculo

//	Vehiculo aux=getVeghiculoBastidor(bastidor)

	// paso veo si tengo que incrementar

	// paso 3 --> return vehiculos[i].addPiezaV

	public boolean addPiezaVehiculo(Pieza p, Integer bastidor) {

		Vehiculo aux = getVehiculoBastidor(bastidor);

		if (aux == null) {
			return false;
		}

		if (p == null) {
			return false;
		}

		for (int i = 0; i < aux.getCont(); i++) {
			if (aux.getPiezaV(i).equals(p)) {
				aux.getPiezaV(i).setStock(aux.getPiezaV(i).getStock() + p.getStock());
				return true;
			}

		}

		return aux.addPiezaV(p);

//		for (int i = 0 ; i < cont ; i++) {
//			if(vehiculos[i].getBastidor() == bastidor) {
//				if(vehiculos[i].addPiezaV(p)) {
//					return true;
//				}else {
//					for(int j = 0 ; j < vehiculos[i].getCont() ; i++) {
//						if(vehiculos[i].getPiezaV(j).equals(p)) {
//							vehiculos[i].getPiezaV(j).setStock(vehiculos[i].getPiezaV(j).getStock()+p.getStock());
//							return true;
//						}
//					}
//				}
//			}
//		}

	}

	public int cuantasPiezasPar() {

		int res = 0;

		for (int i = 0; i < cont; i++) {

			if (vehiculos[i].getCont() % 2 == 0 && vehiculos[i].getCont() != 0) {
				res++;
			}

		}

		return res;

	}

	public String getInfoDerivada(int pos) {

		if (pos < 0 || pos >= cont) {
			return null;
		}

		if (vehiculos[pos].getClass().equals(Coche.class)) {
			return ((Coche) vehiculos[pos]).getColor();
		}

		if (vehiculos[pos].getClass().equals(Camion.class)) {
			return String.valueOf(((Camion) vehiculos[pos]).getTonelaje());
		}

		if (vehiculos[pos].getClass().equals(Moto.class)) {
			return String.valueOf(((Moto) vehiculos[pos]).getPotencia());
		}
		
		return null;

	}

	public String toString() {
		return "Desguace[Nombre: " + nombre + "]";
	}

}
