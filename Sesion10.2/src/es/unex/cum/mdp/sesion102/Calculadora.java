package es.unex.cum.mdp.sesion102;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Calculadora {

	private JFrame frame;
	private JTextField textFieldResultado;
	private Double valor1;
	private Double valor2;
	private int operacion;
	private int ultimo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 538, 357);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(25);
		flowLayout.setHgap(25);
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		textFieldResultado = new JTextField();
		textFieldResultado.setEditable(false);
		textFieldResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldResultado.setToolTipText("Resultado");
		panel.add(textFieldResultado);
		textFieldResultado.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(5, 4, 5, 5));
		
		JButton botonRTC = new JButton("RTC");
		botonRTC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText().substring(0, ultimo-1));
			}
		});
		panel_1.add(botonRTC);
		
		JButton botonCE = new JButton("CE");
		botonCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText("");
				valor1 = 0.0;
				valor2 = 0.0;
				operacion = 0;
				
			}
		});
		panel_1.add(botonCE);
		
		JButton botonClear = new JButton("Cl");
		botonClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText("");
			}
		});
		panel_1.add(botonClear);
		
		JButton botonSignoValor = new JButton("+/-");
		botonSignoValor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Double res = Double.parseDouble(textFieldResultado.getText()) * -1;
				textFieldResultado.setText(res.toString());
			}
		});
		panel_1.add(botonSignoValor);
		
		JButton boton7 = new JButton("7");
		boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "7");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton7);
		
		JButton boton8 = new JButton("8");
		boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "8");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton8);
		
		JButton boton9 = new JButton("9");
		boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "9");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton9);
		
		JButton botonDividir = new JButton("/");
		botonDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					valor1 = Double.parseDouble(textFieldResultado.getText());
					textFieldResultado.setText("");
					operacion = 4;
					}catch(NumberFormatException e2) {
						JOptionPane.showMessageDialog(null, "Error");
					}
			}
		});
		panel_1.add(botonDividir);
		
		JButton boton4 = new JButton("4");
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "4");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton4);
		
		JButton boton5 = new JButton("5");
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "5");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton5);
		
		JButton boton6 = new JButton("6");
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "6");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton6);
		
		JButton botonMultiplicar = new JButton("*");
		botonMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					valor1 = Double.parseDouble(textFieldResultado.getText());
					textFieldResultado.setText("");
					operacion = 3;
					}catch(NumberFormatException e2) {
						JOptionPane.showMessageDialog(null, "Error");
					}
			}
		});
		panel_1.add(botonMultiplicar);
		
		JButton boton1 = new JButton("1");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "1");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton1);
		
		JButton boton2 = new JButton("2");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "2");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton2);
		
		JButton boton3 = new JButton("3");
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "3");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton3);
		
		JButton botonResta = new JButton("-");
		botonResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					valor1 = Double.parseDouble(textFieldResultado.getText());
					textFieldResultado.setText("");
					operacion = 2;
					}catch(NumberFormatException e2) {
						JOptionPane.showMessageDialog(null, "Error");
					}
			}
		});
		panel_1.add(botonResta);
		
		JButton boton0 = new JButton("0");
		boton0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldResultado.setText(textFieldResultado.getText() + "0");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(boton0);
		
		JButton botonDecimal = new JButton(".");
		botonDecimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFieldResultado.setText(textFieldResultado.getText() + ".");
				ultimo = textFieldResultado.getText().length();
			}
		});
		panel_1.add(botonDecimal);
		
		JButton botonResultado = new JButton("=");
		botonResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					valor2 = Double.parseDouble(textFieldResultado.getText());
					Double res = 0.0;
					switch (operacion) {
					case 1:
						res = valor1 + valor2;
						break;
					case 2:
						res = valor1 - valor2;
						break;
					case 3:
						res = valor1 * valor2;
						break;
					case 4:
						try {
							res = valor1 / valor2;
						}catch(ArithmeticException e2) {
							textFieldResultado.setText("Error");
						}
						break;
					}
					textFieldResultado.setText(res.toString());
				}catch(NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}
		});
		panel_1.add(botonResultado);
		
		JButton botonSuma = new JButton("+");
		botonSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				valor1 = Double.parseDouble(textFieldResultado.getText());
				textFieldResultado.setText("");
				operacion = 1;
				}catch(NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		});
		panel_1.add(botonSuma);
	}
}
