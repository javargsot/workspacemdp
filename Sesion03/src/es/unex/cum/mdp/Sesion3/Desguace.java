package es.unex.cum.mdp.Sesion3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeSet;

public class Desguace {

	private String nombre;
	private int tam;
	private ArrayList<Vehiculo> vehiculos;
	private int cont;
	private LinkedList<Proveedor> proveedores;
	private TreeSet<Pieza> piezas;

	public Desguace() {

		nombre = "";
		tam = 0;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();

	}

	public Desguace(String nombre, int tam) {

		this.nombre = nombre;
		this.tam = tam;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();

	}

	public boolean equals(Object o) {
		Desguace d = (Desguace) o;
		return nombre.equals(d.getNombre());
	}

	public String getNombre() {
		return nombre;
	}

	public int getTam() {
		return tam;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTam(int tam) {
		this.tam = tam;
	}

	public boolean addVehiculo(Vehiculo v) {

		if (getVehiculoBastidor(v.getBastidor()) == null) {

			vehiculos.add(v);

		}

		return true;

	}

	public Vehiculo getVehiculoBastidor(Integer bastidor) {

		if (vehiculos.isEmpty()) {
			return null;
		}

		for (int i = 0; i < vehiculos.size(); i++) {
			if (vehiculos.get(i).getBastidor().equals(bastidor)) {
				return vehiculos.get(i);
			}
		}

		return null;

	}

	public boolean addPiezaVehiculo(Pieza p, Integer bastidor) {

		return addPiezaVehiculo(p.getId(), bastidor);

	}
	
	public boolean addPiezaVehiculo(String id, Integer bastidor) {
		
		Vehiculo aux = getVehiculoBastidor(bastidor);
		Pieza p = getPiezaDesguace(id);
		
		if(p == null) {
			return false;
		}
		
		if (aux == null) {
			return false;
		}
		
		for(int i = 0; i < aux.getCont(); i++) {
			if(aux.getPiezaV(i).getId().equals(id)) {
				p.setStock(p.getStock()-1);
			}
		}
		
		return aux.addPiezaV(p);
	}

	public int cuantasPiezasPar() {

		int res = 0;

		for (int i = 0; i < cont; i++) {

			if (vehiculos.get(i).getCont() % 2 == 0 && vehiculos.get(i).getCont() != 0) {
				res++;
			}

		}

		return res;

	}

	public String getInfoDerivada(int pos) {

		if (pos < 0 || pos >= cont) {
			return null;
		}

		if (vehiculos.get(pos).getClass().equals(Coche.class)) {
			return ((Coche) vehiculos.get(pos)).getColor();
		}

		if (vehiculos.get(pos).getClass().equals(Camion.class)) {
			return String.valueOf(((Camion) vehiculos.get(pos)).getTonelaje());
		}

		if (vehiculos.get(pos).getClass().equals(Moto.class)) {
			return String.valueOf(((Moto) vehiculos.get(pos)).getPotencia());
		}

		return null;

	}

	public boolean addPiezaDesguace(Pieza p) {

		if (p == null) {
			return false;
		}

		return piezas.add(p);
	}

	public Pieza getPiezaDesguace(String id) {

		Pieza p = new Pieza();

		while (piezas.iterator().hasNext()) {

			p = piezas.iterator().next();

			if (p.getId().equals(id)) {
				return p;
			}

		}

		return null;
	}

	public boolean addProveedor(Proveedor p) {

		if (p == null) {
			return false;
		}

		return proveedores.add(p);
	}

	public Proveedor getProveedor(String dni) {

		for (int i = 0; i < proveedores.size(); i++) {
			if (proveedores.get(i).getDni().equals(dni)) {
				return proveedores.get(i);
			}
		}

		return null;
	}

	public Pieza getPiezaVehiculo(String idPieza, Integer bastidor) {
		
		for(Vehiculo v : vehiculos) {
			if(v.getBastidor().equals(bastidor)) {
				for(Pieza p : piezas) {
					if(p.getId().equals(idPieza)) {
						return p;
					}
				}
			}
		}
		
		return null;
	}

	public String toString() {
		return "Desguace[Nombre: " + nombre + "]";
	}

}
