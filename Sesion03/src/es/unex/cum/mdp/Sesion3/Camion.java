package es.unex.cum.mdp.Sesion3;

public class Camion extends Vehiculo {

	private Integer tonelaje;
	
	public Camion() {
		super();
		tonelaje = 0;
	}
	
	public Camion(String marca, String modelo, Persona propietario, Integer bastidor, Integer tonelaje) {
		super(marca, modelo, propietario, bastidor);
		this.tonelaje = tonelaje;
	}

	public Camion(Camion c) {
		super(c);
		tonelaje = c.getTonelaje();
	}
	
	public boolean equals(Object o) {
		Camion c = (Camion) o;
		return super.equals(c)&&tonelaje==c.getTonelaje();
	}
	
	public int getTonelaje() {
		return tonelaje;
	}

	public void setTonelaje(Integer tonelaje) {
		this.tonelaje = tonelaje;
	}
	
	public String toString() {
		return "Camion[" + super.toString() + ", Tonelaje: " + tonelaje + "]";
	}
	
}
