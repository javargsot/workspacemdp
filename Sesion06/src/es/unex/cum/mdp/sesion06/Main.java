package es.unex.cum.mdp.sesion06;

import es.unex.cum.mdp.sesion6.ExceptionBastidorNoEncontrado;
import es.unex.cum.mdp.sesion6.ExceptionPiezaNoEncontrada;
import es.unex.cum.mdp.sesion6.Persona;
import es.unex.cum.mdp.sesion6.Pieza;
import es.unex.cum.mdp.sesion6.Vehiculo;

public class Main {

	private Teclado t;
	private Desguace d;

	public static void main(String[] args) {

		Main m = new Main();
		m.t = new Teclado();
		m.d = new Desguace();
		m.play();

	}

	public void play() {
		Vehiculo v = new Vehiculo();
		Pieza p = new Pieza();

		d.addVehiculo(v);

		try {
			
			d.getVehiculoBastidor(v.getBastidor());
			d.getVehiculoBastidor(8);

		} catch (ExceptionBastidorNoEncontrado e) {
			System.out.println(e.getMessage());
		}

		d.addPiezaDesguace(p);
		d.addPiezaVehiculo(p, v.getBastidor());

		try {

			d.getPiezaDesguace(p.getId());
			d.getPiezaDesguace("1");

		} catch (ExceptionPiezaNoEncontrada e) {
			System.out.println(e.getMessage());
		}

		d.getPiezaVehiculo(p.getId(), v.getBastidor());

	}

}
