package es.unex.cum.mdp.sesion06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;

import es.unex.cum.mdp.sesion6.Camion;
import es.unex.cum.mdp.sesion6.Coche;
import es.unex.cum.mdp.sesion6.Empleado;
import es.unex.cum.mdp.sesion6.ExceptionBastidorNoEncontrado;
import es.unex.cum.mdp.sesion6.ExceptionPiezaNoEncontrada;
import es.unex.cum.mdp.sesion6.ExceptionVectorFueraRango;
import es.unex.cum.mdp.sesion6.ExceptionVectorLleno;
import es.unex.cum.mdp.sesion6.ExceptionVectorVacio;
import es.unex.cum.mdp.sesion6.Moto;
import es.unex.cum.mdp.sesion6.Pieza;
import es.unex.cum.mdp.sesion6.Proveedor;
import es.unex.cum.mdp.sesion6.Vehiculo;

public class Desguace {

	private String nombre;
	private int tam;
	private ArrayList<Vehiculo> vehiculos;
	private int cont;
	private LinkedList<Proveedor> proveedores;
	private TreeSet<Pieza> piezas;
	private HashMap<String, Empleado> empleados;

	public Desguace() {

		nombre = "";
		tam = 0;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();
		empleados = new HashMap<String, Empleado>();

	}

	public Desguace(String nombre, int tam) {

		this.nombre = nombre;
		this.tam = tam;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();
		empleados = new HashMap<String, Empleado>();

	}

	public boolean equals(Object o) {
		Desguace d = (Desguace) o;
		return nombre.equals(d.getNombre());
	}

	public String getNombre() {
		return nombre;
	}

	public int getTam() {
		return tam;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTam(int tam) {
		this.tam = tam;
	}

	public boolean addVehiculo(Vehiculo v) {

		try {

			getVehiculoBastidor(v.getBastidor());
			return false;

		} catch (ExceptionBastidorNoEncontrado e) {

			return vehiculos.add(v);

		}

		/*
		 * if (v == null) { return false; }
		 * 
		 * if (getVehiculoBastidor(v.getBastidor()) == null) { return vehiculos.add(v);
		 * }
		 * 
		 * return false;
		 */
	}

	public Vehiculo getVehiculoBastidor(Integer bastidor) throws ExceptionBastidorNoEncontrado {

		if (vehiculos.isEmpty()) {
			throw new ExceptionBastidorNoEncontrado();
		}

		for (Vehiculo v : vehiculos) {
			if (v.getBastidor().equals(bastidor)) {
				return v;
			}
		}

		throw new ExceptionBastidorNoEncontrado();

	}

	public boolean addPiezaVehiculo(Pieza p, Integer bastidor) {

		Vehiculo aux = null;

		try {

			aux = getVehiculoBastidor(bastidor);

		} catch (ExceptionBastidorNoEncontrado e) {

			return false;

		}

		if (p == null) {
			return false;
		}
		try {
			for (int i = 0; i < aux.getCont(); i++) {

				if (aux.getPiezaV(i).equals(p)) {
					aux.getPiezaV(i).setContador(aux.getPiezaV(i).getContador() + p.getContador());
					return true;
				}

			}
		} catch (ExceptionVectorVacio | ExceptionVectorFueraRango e) {
			// Estas excepciones no se pueden producir pues lo controla el for
		}

		try {

			return aux.addPiezaV(p);

		} catch (ExceptionVectorLleno e) {

			return false;

		}

	}

	public boolean addPiezaVehiculo(String id, Integer bastidor) {

		Vehiculo aux = null;
		Pieza p = null;
		Pieza p2 = getPiezaVehiculo(id, bastidor);

		try {

			aux = getVehiculoBastidor(bastidor);

		} catch (ExceptionBastidorNoEncontrado e) {

			return false;

		}

		try {

			p = getPiezaDesguace(id);

		} catch (ExceptionPiezaNoEncontrada e) {

			return false;

		}

		try {

			if (p.getContador() > 0) {

				if (p2 == null) {

					p.setContador(p.getContador() - 1);
					return aux.addPiezaV(new Pieza(p.getId(), p.getNombre(), 1));

				} else {

					p.setContador(p.getContador() - 1);
					p2.setContador(p2.getContador() + 1);
					return true;

				}

			}

		} catch (ExceptionVectorLleno e) {

			return false;

		}

		return false;

	}

	public int cuantasPiezasPar() {

		int res = 0;

		for (int i = 0; i < cont; i++) {

			if (vehiculos.get(i).getCont() % 2 == 0 && vehiculos.get(i).getCont() != 0) {
				res++;
			}

		}

		return res;

	}

	public String getInfoDerivada(int pos) {

		if (pos < 0 || pos >= cont) {
			return null;
		}

		if (vehiculos.get(pos).getClass().equals(Coche.class)) {
			return ((Coche) vehiculos.get(pos)).getColor();
		}

		if (vehiculos.get(pos).getClass().equals(Camion.class)) {
			return String.valueOf(((Camion) vehiculos.get(pos)).getTonelaje());
		}

		if (vehiculos.get(pos).getClass().equals(Moto.class)) {
			return String.valueOf(((Moto) vehiculos.get(pos)).getPotencia());
		}

		return null;

	}

	public boolean addPiezaDesguace(Pieza p) {

		try {

			getPiezaDesguace(p.getId());
			return false;

		} catch (ExceptionPiezaNoEncontrada e) {

			return piezas.add(p);

		}

	}

	public Pieza getPiezaDesguace(String id) throws ExceptionPiezaNoEncontrada {

		if (!piezas.isEmpty()) {
			Pieza p = new Pieza();
			Iterator it = piezas.iterator();

			while (it.hasNext()) {

				p = (Pieza) it.next();

				if (p.getId().equals(id)) {
					return p;
				}
			}
		}

		throw new ExceptionPiezaNoEncontrada();
	}

	public boolean addProveedor(Proveedor p) {

		if (p == null) {
			return false;
		}

		if (proveedores.isEmpty()) {
			return proveedores.add(p);
		}
		if (getProveedor(p.getDni()) == null) {
			return proveedores.add(p);
		}
		return false;

	}

	public Proveedor getProveedor(String dni) {

		for (Proveedor p : proveedores) {
			if (p.getDni().equals(dni)) {
				return p;
			}
		}

		return null;
	}

	public Pieza getPiezaVehiculo(String idPieza, Integer bastidor) {

		Vehiculo aux = null;

		if (idPieza == null) {
			return null;
		}

		try {

			aux = getVehiculoBastidor(bastidor);

		} catch (ExceptionBastidorNoEncontrado e) {
			return null;
		}

		try {

			for (int i = 0; i < aux.getCont(); i++) {
				if (aux.getPiezaV(i).getId().equals(idPieza)) {
					return aux.getPiezaV(i);
				}
			}

		} catch (ExceptionVectorVacio e) {

			return null;

		} catch (ExceptionVectorFueraRango e) {

		}

		return null;

	}

	@SuppressWarnings("unchecked")
	public Vehiculo mayorStock() {

		if (!vehiculos.isEmpty()) {
			Collections.sort(vehiculos);
			return (Vehiculo) vehiculos.get(vehiculos.size() - 1);
		}
		return null;

	}

	public boolean addEmpleado(Empleado e) {

		if (e == null) {
			return false;
		}

		if (!empleados.containsKey(e.getDni())) {
			empleados.put(e.getDni(), e);
			return true;
		}

		return false;
	}

	public Empleado getEmpleado(String dni) {

		if (dni == null) {
			return null;
		}

		return (Empleado) empleados.get(dni);
	}

	public double getMediaSueldo() {

		double res = 0.0;

		if (!empleados.isEmpty()) {

			Iterator it = empleados.entrySet().iterator();

			while (it.hasNext()) {

				Map.Entry m = (Map.Entry) it.next();
				Empleado e = (Empleado) m.getValue();
				res = res + e.getSueldo();

			}

			return res / empleados.size();

		}

		return 0.0;

	}

	public String toString() {
		return "Desguace[Nombre: " + nombre + "]";
	}

}
