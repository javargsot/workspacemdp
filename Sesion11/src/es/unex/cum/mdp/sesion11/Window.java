package es.unex.cum.mdp.sesion11;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Window {

	private JFrame frmGestionarDesguace;
	private Desguace d = new Desguace();
	private Pieza piezaActual;
	private Vehiculo vehiculoActual;
	private DialogoAnyadirPieza np = new DialogoAnyadirPieza(frmGestionarDesguace);
	private DiaologoIntroducirVehiculo nv = new DiaologoIntroducirVehiculo(frmGestionarDesguace);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmGestionarDesguace.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionarDesguace = new JFrame();
		frmGestionarDesguace.setTitle("Gestionar desguace");
		frmGestionarDesguace.setBounds(100, 100, 450, 300);
		frmGestionarDesguace.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frmGestionarDesguace.getContentPane().add(panel, BorderLayout.WEST);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblNewLabel = new JLabel("Piezas");
		panel.add(lblNewLabel);

		JComboBox comboBox = new JComboBox();
		panel.add(comboBox);

		JLabel lblVehiculos = new JLabel("Vehiculos");
		panel.add(lblVehiculos);

		JComboBox comboBox_1 = new JComboBox();
		panel.add(comboBox_1);

		JPanel panel_1 = new JPanel();
		frmGestionarDesguace.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.NORTH);

		JLabel lblNewLabel_1 = new JLabel("Información");
		panel_2.add(lblNewLabel_1);

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.CENTER);

		JList list = new JList();
		list.setVisibleRowCount(10);
		panel_3.add(list);

		JMenuBar menuBar = new JMenuBar();
		frmGestionarDesguace.setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);

		JMenu mnOperaciones = new JMenu("Operaciones");
		menuBar.add(mnOperaciones);

		JMenuItem mntmAadirPieza = new JMenuItem("Añadir Pieza");
		mntmAadirPieza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pieza p = np.Mostrar();
				if (p != null) {
					p.setId(String.valueOf(d.getPiezas().size() + 1));
					if(d.addPiezaDesguace(p)){
						comboBox.addItem(p.getNombre());
					}else {
						JOptionPane.showMessageDialog( null,"Ha habido un problema.");
					}
					
				}
			}
		});
		mnOperaciones.add(mntmAadirPieza);

		JMenuItem mntmNewMenuItem = new JMenuItem("Añadir Vehiculo");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nv.Mostrar(d);
			}
		});
		mnOperaciones.add(mntmNewMenuItem);

		JMenuItem mntmAadirProveedor = new JMenuItem("Añadir Proveedor");
		mnOperaciones.add(mntmAadirProveedor);

		JMenuItem mntmAadirEmpleado = new JMenuItem("Añadir Empleado");
		mnOperaciones.add(mntmAadirEmpleado);

		JMenuItem mntmVerVehiculos = new JMenuItem("Ver Vehiculos");
		mnOperaciones.add(mntmVerVehiculos);

		JMenuItem mntmVerPiezas = new JMenuItem("Ver Piezas");
		mnOperaciones.add(mntmVerPiezas);

		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);

		JMenuItem mntmAcerca = new JMenuItem("Acerca");
		mnAyuda.add(mntmAcerca);

	}

}
