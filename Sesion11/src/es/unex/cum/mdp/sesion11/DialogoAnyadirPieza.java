package es.unex.cum.mdp.sesion11;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DialogoAnyadirPieza extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private Pieza pieza = null;

	/**
	 * Create the dialog.
	 */
	public DialogoAnyadirPieza(JFrame f) {
		super(f, "Datos Pieza", true);
		setModal(true);
		setTitle("Introducir Pieza");
		setBounds(100, 100, 359, 166);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		{
			JLabel lblNombre = new JLabel("Nombre");
			contentPanel.add(lblNombre);
		}
		{
			textField = new JTextField();
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JLabel lblNmeroDePiezas = new JLabel("Número de Piezas");
			contentPanel.add(lblNmeroDePiezas);
		}
		{
			textField_1 = new JTextField();
			contentPanel.add(textField_1);
			textField_1.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						pieza = new Pieza("", textField.getText(), Integer.parseInt(textField_1.getText()));
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public Pieza Mostrar() {
		textField.setText("");
		textField_1.setText("");
		// se establece el valor de CT a vacio
		pieza = null;
		// pieza es el atributo
		// hace visible el diálogo --> la ventana
		setVisible(true);
		// cuando se cierra la pieza introducida está en "pieza"
		return pieza;
	}

}
