package es.unex.cum.mdp.sesion11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;

public class Desguace {

	private String nombre;
	private int tam;
	private ArrayList<Vehiculo> vehiculos;
	private int cont;
	private LinkedList<Proveedor> proveedores;
	private TreeSet<Pieza> piezas;
	private HashMap<String, Empleado> empleados;

	public Desguace() {

		nombre = "";
		tam = 0;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();
		empleados = new HashMap<String, Empleado>();

	}

	public Desguace(String nombre, int tam) {

		this.nombre = nombre;
		this.tam = tam;

		vehiculos = new ArrayList<Vehiculo>();
		cont = 0;
		proveedores = new LinkedList<Proveedor>();
		piezas = new TreeSet<Pieza>();
		empleados = new HashMap<String, Empleado>();

	}

	public boolean equals(Object o) {
		Desguace d = (Desguace) o;
		return nombre.equals(d.getNombre());
	}

	public String getNombre() {
		return nombre;
	}

	public int getTam() {
		return tam;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTam(int tam) {
		this.tam = tam;
	}

	public ArrayList<Vehiculo> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public LinkedList<Proveedor> getProveedores() {
		return proveedores;
	}

	public void setProveedores(LinkedList<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}

	public TreeSet<Pieza> getPiezas() {
		return piezas;
	}

	public void setPiezas(TreeSet<Pieza> piezas) {
		this.piezas = piezas;
	}

	public HashMap<String, Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(HashMap<String, Empleado> empleados) {
		this.empleados = empleados;
	}

	public boolean addVehiculo(Vehiculo v) {

		if (v == null) {
			return false;
		}

		if (getVehiculoBastidor(v.getBastidor()) == null) {
			return vehiculos.add(v);
		}

		return false;

	}

	public Vehiculo getVehiculoBastidor(Integer bastidor) {

		if (vehiculos.isEmpty()) {
			return null;
		}

		for (Vehiculo v : vehiculos) {
			if (v.getBastidor().equals(bastidor)) {
				return v;
			}
		}

		return null;

	}

	public boolean addPiezaVehiculo(Pieza p, Integer bastidor) {

		Vehiculo aux = getVehiculoBastidor(bastidor);

		if (aux == null) {
			return false;
		}

		if (p == null) {
			return false;
		}

		for (int i = 0; i < aux.getCont(); i++) {
			if (aux.getPiezaV(i).equals(p)) {
				aux.getPiezaV(i).setStock(aux.getPiezaV(i).getStock() + p.getStock());
				return true;
			}

		}

		return aux.addPiezaV(p);

	}

	public boolean addPiezaVehiculo(String id, Integer bastidor) {

		Vehiculo aux = getVehiculoBastidor(bastidor);
		Pieza p = getPiezaDesguace(id);
		Pieza p2 = getPiezaVehiculo(id, bastidor);

		if (aux == null) {
			return false;
		}

		if (p == null) {
			return false;
		}

		if (p.getStock() > 0) {

			if (p2 == null) {

				p.setStock(p.getStock() - 1);
				return aux.addPiezaV(new Pieza(p.getId(), p.getNombre(), 1));

			} else {

				p.setStock(p.getStock() - 1);
				p2.setStock(p2.getStock() + 1);
				return true;

			}

		}

		return false;
	}

	public int cuantasPiezasPar() {

		int res = 0;

		for (int i = 0; i < cont; i++) {

			if (vehiculos.get(i).getCont() % 2 == 0 && vehiculos.get(i).getCont() != 0) {
				res++;
			}

		}

		return res;

	}

	public String getInfoDerivada(int pos) {

		if (pos < 0 || pos >= cont) {
			return null;
		}

		if (vehiculos.get(pos).getClass().equals(Coche.class)) {
			return ((Coche) vehiculos.get(pos)).getColor();
		}

		if (vehiculos.get(pos).getClass().equals(Camion.class)) {
			return String.valueOf(((Camion) vehiculos.get(pos)).getTonelaje());
		}

		if (vehiculos.get(pos).getClass().equals(Moto.class)) {
			return String.valueOf(((Moto) vehiculos.get(pos)).getPotencia());
		}

		return null;

	}

	public boolean addPiezaDesguace(Pieza p) {

		if (p == null) {
			return false;
		}

		return piezas.add(p);
	}

	public Pieza getPiezaDesguace(String id) {

		if (id == null) {
			return null;
		}

		Pieza p = new Pieza();
		Iterator<Pieza> it = piezas.iterator();

		while (it.hasNext()) {

			p = (Pieza) it.next();

			if (p.getId().equals(id)) {
				return p;
			}

		}

		return null;
	}

	public boolean addProveedor(Proveedor p) {

		if (p == null) {
			return false;
		}
		
		if(proveedores.isEmpty()) {
			return proveedores.add(p);
		}
		
		if(getProveedor(p.getDni()) == null) {
			return proveedores.add(p);			
		}

		return false;
		
	}

	public Proveedor getProveedor(String dni) {

		if (dni == null) {
			return null;
		}

		for (Proveedor p : proveedores) {
			if (p.getDni().equals(dni)) {
				return p;
			}
		}

		return null;
	}

	public Pieza getPiezaVehiculo(String idPieza, Integer bastidor) {

		if (idPieza == null) {
			return null;
		}

		Vehiculo aux = getVehiculoBastidor(bastidor);

		if (aux == null) {
			return null;
		}

		for (int i = 0; i < aux.getCont(); i++) {
			if (aux.getPiezaV(i).getId().equals(idPieza)) {
				return aux.getPiezaV(i);
			}
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public Vehiculo mayorStock() {
		if (!vehiculos.isEmpty()) {
			Collections.sort(vehiculos);
			return (Vehiculo) vehiculos.get(vehiculos.size() - 1);
		}
		return null;

	}

	public boolean addEmpleado(Empleado e) {

		if (e == null) {
			return false;
		}

		if (!empleados.containsKey(e.getDni())) {
			empleados.put(e.getDni(), e);
			return true;
		}

		return false;
	}

	public Empleado getEmpleado(String dni) {

		if (dni == null) {
			return null;
		}

		return (Empleado) empleados.get(dni);
	}

	public double getMediaSueldo() {

		double res = 0.0;

		if (!empleados.isEmpty()) {

			Iterator it = empleados.entrySet().iterator();
			
			while(it.hasNext()) {
				
				Map.Entry m = (Map.Entry)it.next();
				Empleado e=(Empleado)m.getValue();
				res = res + e.getSueldo();
				
			}

		return res / empleados.size();
		
		}
		
		return 0.0;
		
	}

	public String toString() {
		return "Desguace[Nombre: " + nombre + "]";
	}

}
