package es.unex.cum.mdp.sesion11;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DiaologoIntroducirVehiculo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldModelo;
	private JTextField textFieldBastidor;
	private JTextField textFieldMarca;
	private Desguace aux = null;

	/**
	 * Create the dialog.
	 */
	public DiaologoIntroducirVehiculo(JFrame f) {
		super(f, "Datos Vehiculo", false);
		setTitle("Introducir Vehiculo");
		setBounds(100, 100, 418, 176);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		{
			JLabel lblMarca = new JLabel("Marca");
			contentPanel.add(lblMarca);
		}
		{
			textFieldMarca = new JTextField();
			contentPanel.add(textFieldMarca);
			textFieldMarca.setColumns(10);
		}
		{
			JLabel lblModelo = new JLabel("Modelo");
			contentPanel.add(lblModelo);
		}
		{
			textFieldModelo = new JTextField();
			contentPanel.add(textFieldModelo);
			textFieldModelo.setColumns(10);
		}
		{
			JLabel lblBastidor = new JLabel("Bastidor");
			contentPanel.add(lblBastidor);
		}
		{
			textFieldBastidor = new JTextField();
			contentPanel.add(textFieldBastidor);
			textFieldBastidor.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Vehiculo v = new Vehiculo(textFieldMarca.getText(), textFieldModelo.getText(), new Persona(),
								Integer.parseInt(textFieldBastidor.getText()));
						if(aux.addVehiculo(v)) {
							JOptionPane.showMessageDialog(null, "Todo ha ido bien");
						}else {
							JOptionPane.showMessageDialog(null, "Ha habido un error");
						}
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public void Mostrar(Desguace d) {
		textFieldModelo.setText("");
		textFieldBastidor.setText("");
		// se establece el valor de CT a vacio
		textFieldMarca.setText("");
		// se establece el valor de CT a vacio
		aux = d;
		// hace visible el diálogo --> la ventana
		setVisible(true);
	}

}
