package es.unex.cum.mdp.sesion11;

import java.util.Iterator;

public class Main {
	private Teclado t;
	private Desguace d;
	
	public static void main(String[] args){
		Main m=new Main();
		m.exec();
	}
	
	public void exec(){
		d=new Desguace();
		t=new Teclado();
		Pieza p=new Pieza("1", "Bujia",5);
		System.out.println(d.addVehiculo(new Moto("Honda","CBR600",new Persona(), 1, 75)));
		System.out.println(d.addVehiculo(new Coche("Subaru","Impreza",new Persona(), 2, "Azul")));
		System.out.println(d.addVehiculo(new Camion("Scania","C1000",new Persona(), 3, 4)));
		System.out.println(d.addPiezaDesguace(new Pieza("0", "Bujia", 50)));
		System.out.println(d.addPiezaDesguace(new Pieza("2", "Transmision", 20)));
		System.out.println(d.addPiezaDesguace(new Pieza("1", "Volante", 20)));
		System.out.println(d.addPiezaDesguace(new Pieza("3", "Luz", 20)));
		
		
		System.out.println(d.addPiezaVehiculo("1", 2));
		System.out.println(d.addPiezaVehiculo("2", 2));
		System.out.println(d.addPiezaVehiculo("3", 2));
		System.out.println(d.addPiezaVehiculo("0", 1));
		System.out.println(d.addPiezaVehiculo("2", 1));
		System.out.println(d.mayorStock());
		
//		Iterator it= d.getPiezas().iterator();
//		while(it.hasNext()){
//			Pieza paux=(Pieza)it.next();
//			System.out.println(paux.toString());
//		}
	}
}
