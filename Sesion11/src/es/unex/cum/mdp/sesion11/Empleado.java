package es.unex.cum.mdp.sesion11;

import java.util.Date;

public class Empleado extends Persona{

	private double sueldo;
	private Date fecha;
	
	public Empleado() {	
		super();
		
		sueldo = 0.0;
		fecha = new Date();
		
	}
	
	public Empleado(String nombre, String dni, Integer edad, double sueldo, Date fecha) {
		super(nombre, dni, edad);
		
		this.sueldo = sueldo;
		this.fecha = fecha;
		
	}
	
	public Empleado(Empleado e) {
		super(e);
		
		sueldo = e.getSueldo();
		fecha = e.getFecha();
		
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
}
