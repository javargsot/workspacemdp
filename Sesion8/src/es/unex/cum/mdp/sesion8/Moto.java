package es.unex.cum.mdp.sesion8;

public class Moto extends Vehiculo {
	
	private Integer potencia;
	
	public Moto() {
		super();
		potencia = 0;
	}
	
	public Moto(String marca, String modelo, Persona propietario, Integer bastidor, Integer potencia) {
		super(marca, modelo, propietario, bastidor);
		this.potencia = potencia;
	}
	
	public Moto(Moto m) {
		super(m);
		potencia = m.getPotencia();
	}
	
	public boolean equals(Object o) {
		Moto m = (Moto) o;
		return super.equals(m)&&potencia==m.getPotencia();
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(Integer potencia) {
		this.potencia = potencia;
	}
	
	public String toString() {
		return "Moto[" + super.toString() + ", Potencia: " + potencia + "]";
	}

}
