package es.unex.cum.mdp.sesion8;

public class FactoriaVehiculo {

	private static Vehiculo v = null;
	private static Persona propietario = new Persona();

	public static Vehiculo buildCoche(String marca, String modelo, Integer bastidor, String color, int tipo) {

		v = new Coche(marca, modelo, propietario, bastidor, color);
		return v;

	}

	public static Vehiculo buildMoto(String marca, String modelo, Integer bastidor, int potencia, int tipo) {

		v = new Moto(marca, modelo, propietario, bastidor, potencia);
		return v;

	}
	
	public static Vehiculo buildCamion(String marca, String modelo, Integer bastidor, int tonelaje, int tipo) {
		
		v = new Camion(marca, modelo, propietario, bastidor, tonelaje);
		return v;
		
	}

}
