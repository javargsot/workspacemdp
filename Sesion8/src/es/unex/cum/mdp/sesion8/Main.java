package es.unex.cum.mdp.sesion8;

import java.io.IOException;

public class Main {
	private DesguaceSingleton d;

	public static void main(String[] args) {
		Main m = new Main();
		m.ejecutar();
	}

	public void ejecutar() {
		d = DesguaceSingleton.getInstance();
		Teclado t = new Teclado();
		int opcion = 0;
		do {
			try {
				opcion = t.Menu(new String[] { "1. Añadir Pieza", "2. Añadir Vehiculo", "3. Añadir Pieza Vehiculo",
						"4. Mostrar Piezas/Vehiculo", "5. Salir" }, 1, 5);
				switch (opcion) {
				case 1:
					String id = t.literalConString("Dame el id");
					try {
						Pieza p2 = d.getPiezaDesguace(id);
						System.out.println("que ya esta");
					} catch (ExceptionPiezaNoEncontrada e) {
						String nombre = t.literalConString("Dame elnombre");
						int stock = t.literalConEntero("Dame el número");
						Pieza paux = new Pieza(id, nombre, stock);
						d.addPiezaDesguace(paux);
					}
					break;
				case 2:
					Integer bastidor = t.literalConEntero("Dame el bástidor");
					String marca = t.literalConString("Dame el marca");
					String modelo = t.literalConString("Dame el modelo");

					int tipo = t.literalConEntero("1. Coche 2. Moto 3. Camion");

					if(tipo == 1) {
						String color = t.literalConString("Dame el color");
						Vehiculo v = FactoriaVehiculo.buildCoche(marca, modelo, bastidor, color, tipo);
						d.addVehiculo(v);
					}else if(tipo == 2) {
						int potencia = t.literalConEntero("Dame la potencia");
						Vehiculo v = FactoriaVehiculo.buildMoto(marca, modelo, bastidor, potencia, tipo);
						d.addVehiculo(v);
					}else if(tipo == 3) {
						int tonelaje = t.literalConEntero("Dame el tonelaje");
						Vehiculo v = FactoriaVehiculo.buildCamion(marca, modelo, bastidor, tonelaje, tipo);
						d.addVehiculo(v);
					}
					
					break;
				case 3:
					String id2 = t.literalConString("Dame el id");
					Integer b2 = t.literalConEntero("Dame el bástidor");
					
					try {
						
					d.addPiezaVehiculo(id2, b2);
					
					}catch(ExceptionVectorLleno | ExceptionPiezaNoEncontrada | ExceptionBastidorNoEncontrado e) {
						System.out.println(e.getMessage());
					}
					
					break;
				case 4:
					System.out.println(d.getPiezas().toString());
					break;
				}
			} catch (IOException e) {
				opcion = 0;
				System.out.println("Excepción");
			}
		} while (opcion != 5);
	}
}