package es.unex.cum.mdp.sesion4;

public class Pieza implements Comparable{

	private String nombre;
	private String id;
	private Integer stock;

	public Pieza() {

		nombre = "";
		id = "";
		stock = 0;

	}

	public Pieza(String id, String nombre, Integer stock) {

		this.nombre = nombre;
		this.id = id;
		this.stock = stock;

	}

	public Pieza(Pieza p) {

		id = p.getId();
		nombre = p.getNombre();
		stock = p.getStock();

	}

	public boolean equals(Object o) {

		boolean res = false;
		Pieza p = (Pieza) o;

		if (id.equals(p.getId())) {

			res = true;

		}

		return res;

	}
	
	public int compareTo(Object o) {
		
		Pieza p = (Pieza) o;
				
		return id.compareTo(p.getId());
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String toString() {
		return "Pieza [id=" + id + ", nombre=" + nombre + ", stock=" + stock + "]";
	}

}
