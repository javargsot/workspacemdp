package es.unex.cum.mdp.sesion4;

public class Vehiculo implements Comparable {

	protected String marca;
	protected String modelo;
	protected Persona propietario;
	protected Integer bastidor;

	private Pieza[] piezas;
	private int cont;

	public Vehiculo() {

		marca = "";
		modelo = "";
		propietario = new Persona();
		bastidor = 0;

		piezas = new Pieza[3];
		cont = 0;

	}

	public Vehiculo(String marca, String modelo, Persona propietario, Integer bastidor) {

		this.marca = marca;
		this.modelo = modelo;
		this.propietario = propietario;
		this.bastidor = bastidor;

		piezas = new Pieza[3];
		cont = 0;

	}

	public Vehiculo(Vehiculo v) {

		marca = v.getMarca();
		modelo = v.getModelo();
		propietario = v.getPropietario();
		bastidor = v.getBastidor();

		piezas = new Pieza[3];
		cont = 0;

	}

	public int compareTo(Object o) {

		Vehiculo v = (Vehiculo) o;

		return cont - v.getCont();
		
	}

	public boolean equals(Object o) {

		boolean res = false;
		Vehiculo v = (Vehiculo) o;

		if (marca.equals(v.getMarca())) {
			if (modelo.equals(v.getModelo())) {
				if (propietario.equals(v.getPropietario())) {
					res = true;
				}
			}
		}

		return res;

	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Persona getPropietario() {
		return propietario;
	}

	public Integer getBastidor() {
		return bastidor;
	}

	public void setPropietario(Persona propietario) {
		this.propietario = propietario;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public void setBastidor(Integer bastidor) {
		this.bastidor = bastidor;
	}

	public Pieza getPiezaV(int pos) {

		if (pos < 0 || pos >= piezas.length) {

			return null;

		}

		return piezas[pos];

	}

	public boolean addPiezaV(Pieza p) {

		if (cont == 0) {
			piezas[cont] = p;
			cont++;
			return true;
		} else {

			for (int i = 0; i < cont; i++) {

				if (p.getId().equals(piezas[i].getId())) {
					return false;
				}
			}
		}

		if (cont == piezas.length) {
			return false;
		}

		piezas[cont] = p;
		cont++;

		return true;

	}

	public String toString() {
		return "Marca: " + marca + ", Modelo: " + modelo + ", Propietario: " + propietario + ", Bastidor: " + bastidor;
	}

}
