package es.unex.cum.mdp.sesion4;

public class Persona {
	
	private String nombre;
	private String dni;
	private Integer edad;
	
	public Persona() {
		
		nombre = "";
		dni = "";
		edad = 0;
		
	}
	
	public Persona(String nombre, String dni, Integer edad) {
		
		this.nombre = nombre;
		this.dni = dni;
		this.edad = edad;
		
	}
	
	public Persona(Persona p) {
		
		nombre = p.getNombre();
		dni = p.getDni();
		edad = p.getEdad();
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	public String toString() {
		return "[Nombre: " + nombre + ", DNI: " + dni +", Edad: " + edad + "]";
	}

}
